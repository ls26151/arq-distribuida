import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import Network.Frame;

public class Main {

	public static void main (String[] args) {
		String updates = "";
		String host = "10.10.1.109";
		int port = 8811;
		Frame frameTX = new Frame(null,null,"CLIENT-WRITE",0,"w(26:23),w(34:27),w(25:43),w(21:31)",0);
//		Frame frameTX = new Frame(null,null,"CLIENT-READ",0,"r(12),r(1),r(3),r(15)",0);
		try {
			Socket conn = new Socket(host,port);
			ObjectOutputStream oStream = new ObjectOutputStream(conn.getOutputStream());
			ObjectInputStream iStream = new ObjectInputStream(conn.getInputStream());
			
			oStream.writeObject(frameTX);
			Frame frameRX = (Frame) iStream.readObject();
			System.out.println("Result: "+frameRX.frame_message);

			oStream.close();
			iStream.close();
			conn.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	
}
