
#ifndef SERVER_H_
#define SERVER_H_

#define STRING_BUFFER_SIZE 100
#define CONF_NICKNAME_SIZE 20
#define MYTRUE 10
#define MYFALSE -10


typedef struct{ 
	char operator[CONF_NICKNAME_SIZE];
	char mensaje[STRING_BUFFER_SIZE];
	long timestamp;
} MensajeDisco;


#endif