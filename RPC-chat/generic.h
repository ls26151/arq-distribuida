/****************************************************************
*       @File:          shared.h
*		@Desc: 			Libreria para las funciones / constantes compartidas
*       @Author:        Guille Rodríguez        LS26151
*       @Date:          2014/2015
****************************************************************/

#ifndef GENERIC_H_
#define GENERIC_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#define MYTRUE 10
#define MYFALSE -10
#define FD_SCREEN 1
#define FD_KEYBOARD 0
#define NODE_HOSTADDR_SIZE 12
#define NODE_HOSTPORT_SIZE 6
#define STRING_BUFFER_SIZE 100
#define CONF_NICKNAME_SIZE 20
#define UPDATE_INTERVAL 1


int validateIPAddress(char *ipaddress);
int validatePortNumber(char *port);

typedef struct{
	char host_addr[12];
	char host_port[6];
} Node;

typedef struct{
	long msgNumber;
	char nickname[50];	
} Config;


Config new_Config(void);
Config load_config(char *name);

Node new_Node(void);
Node load_server_config(char *);

void close_correct(void);
void close_error(void);
void print_help(void);

int shellParser(char *,Config, Node);
void getMessages(void);

void rsi_handler(int);
#endif