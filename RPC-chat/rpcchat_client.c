/****************************************************************
*       @File:          rpcchat_client.c
*		@Desc: 			Cliente para el chat RPC
*       @Author:        Guille Rodríguez        LS26151
*       @Date:          2014/2015
****************************************************************/
#include "generic.h"
#include "rpcchat.h"

void sendMessage(message,Node);


// GLOBALS
Node server;
Config config;
static pthread_mutex_t mtx_alarm = PTHREAD_MUTEX_INITIALIZER;	
static pthread_mutex_t mtx_tstamp= PTHREAD_MUTEX_INITIALIZER;	


void *thread_update_messages(void *arg) {
	// Aprovechamos la variable result para eliminar el warning del arg unused
	int result = strlen(arg);
	int bool =  MYTRUE;
	while ( bool == MYTRUE ) {
		result = pthread_mutex_lock(&mtx_alarm);
		if ( result != 0 ) {
			printf("[ERROR] - No se pudo bloquear el recurso. \n");
			close_error();
		} else {
			getMessages();
			alarm(UPDATE_INTERVAL);
		}
	}
	pthread_exit(NULL);
}

int main (int argc, char *argv[]) {
	// Redefinimos SIGNALS
	signal(SIGALRM,rsi_handler);
	signal(SIGINT,rsi_handler);
	signal(SIGTERM,rsi_handler);
	// Creamos variable del thread
	pthread_t thUpdateMessages;

	printf("\n** CHAT RPC v0.1 by GRodriguez **\n");
	char buffer[STRING_BUFFER_SIZE];

	if ( argc != 3 ) {
		print_help();
		close_error();
	}

	server = load_server_config(argv[1]);
	config = load_config(argv[2]);
	int result = 0;
	result = pthread_create(&thUpdateMessages,NULL,thread_update_messages,"[INFO] Connecting chat...\n\0");
	if ( result != 0 ) {
		printf("[ERROR] - No se pudo lanzar el thread\n");
		close_error();
	}
	
	printf("\n** Welcome to RPCChat v0.1 client **\n\n");
	printf("[INFO] Server selected %s\n",server.host_addr);
	printf("[INFO] Your nickname is %s \n",config.nickname);

	int readed_bytes;
	int menuEXIT = MYFALSE;
	while (menuEXIT == MYFALSE ) {
		readed_bytes = 0;
		bzero(buffer,STRING_BUFFER_SIZE);
		sprintf(buffer,"%s --> ",config.nickname);
		write(FD_SCREEN, buffer, strlen(buffer));
		readed_bytes = read(FD_KEYBOARD,&buffer,STRING_BUFFER_SIZE);
		buffer[readed_bytes - 1] = '\0';
		menuEXIT = shellParser(buffer,config,server);
	}
	printf("\nDisconected from chat\nBye bye.\n");
	close_correct();
	return 1;
}


void rsi_handler(int signal) {
	// Desbloqueamos Mutex para que el Thread solicite mensajes
	if ( signal == SIGALRM ) { pthread_mutex_unlock(&mtx_alarm); }
	if ( signal == SIGTERM || signal == SIGINT ) { close_correct(); }
}

int shellParser(char *string,Config cfg, Node serv) {
	int EXIT = MYFALSE;
	
	if ( strcmp(string,":disconnect") == 0 ) {
		EXIT = MYTRUE;
	} else {
		message msg;
		msg.timestamp = 0;
		msg.operator= malloc(CONF_NICKNAME_SIZE * sizeof(char));
		msg.message = malloc(STRING_BUFFER_SIZE * sizeof(char));
		strcpy(msg.operator,cfg.nickname);
		strcpy(msg.message,string);
	
		sendMessage(msg, serv);
		
		free(msg.operator);
		free(msg.message);
	}
	return EXIT;
}

void getMessages(void) {
	//printf("Getting messages\n"); // DEBUG
	CLIENT *clnt;
	historic *result;
	long tstamp = config.msgNumber;
	if ( pthread_mutex_lock(&mtx_tstamp) == 0 ) {
		
		clnt = clnt_create (server.host_addr, rpcchat, CHATMESSAGING, "udp");
		if ( clnt == NULL ) {
			clnt_pcreateerror(server.host_addr);
			close_error();
		}
	
		result = getchat_1(&tstamp,clnt);
		if ( result == (historic *) NULL) {
			clnt_perror (clnt,"call failed");
			close_error();
		}
		// printf("Numero de mensajes recibidos: %d \n",result->list.list_len); // DEBUG
		
		int i = 0;
		
		for ( i = 0; i< result->list.list_len; i++) {
			if ( strcmp(config.nickname,result->list.list_val[i].operator) != 0 ) {
				printf("%s: %s \n",result->list.list_val[i].operator,result->list.list_val[i].message);
			}
			// Evitamos que si nos lo ha enviado desordenado, no se muestre un paquete.
			if ( config.msgNumber < result->list.list_val[i].timestamp ) { config.msgNumber = result->list.list_val[i].timestamp; } 
		}
		
		
		// Falta añadir el write para que aparezca otra vez la escritura "operator --> "
		
		
		clnt_destroy(clnt);
		pthread_mutex_unlock(&mtx_tstamp);
	}
	
	
}

void sendMessage(message msg, Node serv) {
	CLIENT *clnt;
	void *result;
	
	clnt = clnt_create(serv.host_addr, rpcchat, CHATMESSAGING, "udp");
	if ( clnt == NULL ) {
		clnt_pcreateerror (serv.host_addr);
		printf("[ERROR] No se pudo enviar el mensaje\n");
		close_error();
	}
	result = writetochat_1(&msg, clnt);
	if ( result == (void *) NULL ) {
		clnt_perror (clnt, "call fail");
		printf("[ERROR] No se recibio respuesta del servidor\n");
		close_error();
	}
	clnt_destroy(clnt);
}
void close_correct() {
	printf("\n");
	pthread_mutex_destroy(&mtx_alarm);
	pthread_mutex_destroy(&mtx_tstamp);
	exit(EXIT_SUCCESS);
}
void close_error() {
	printf("\n");
	pthread_mutex_destroy(&mtx_alarm);
	pthread_mutex_destroy(&mtx_tstamp);
	exit(EXIT_FAILURE);
}




