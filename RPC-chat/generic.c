#include "generic.h"


int validateIPAddress(char *ipaddress) {
	struct sockaddr_in sa;
	int result = inet_pton(AF_INET, ipaddress, &(sa.sin_addr));
	if (result == 1 ) {
		result = MYTRUE;
	} else {
		result = MYFALSE;
	}
	return result;
}
int validatePortNumber(char *ipport) {
	if ( strlen(ipport) < 4 || strlen(ipport) > 5 ) { return MYFALSE; }

	int size = strlen(ipport);
	int i;
	for (i = 0; i<size; i++) {
		// ASCII Table --> 48 ==> 0  57 ==> 9
		if ( ipport[i] < 48 || ipport[i] > 57 ) {
			return MYFALSE;
		}
	}
	
	int port = atoi(ipport);
	if ( port > 1023 && port < 65535 ) {
		return MYTRUE;
	}
	return MYFALSE;
}

Config load_config(char *nickname) {
	Config cfg = new_Config();
	int size = strlen(nickname);
	if (size < 3 || size > CONF_NICKNAME_SIZE) { 
		printf("[ERROR] - Invalid nickname length.\n");
		print_help();
		close_error();
	}
	int i;
	// Check non valid chars in nickname 
	// ASCII Table --> A-Z && a-z
	for ( i = 0; i<size; i++) {
		if ( ! ((nickname[i] >= 65 && nickname[i] <= 90) || (nickname[i]>=97 && nickname[i]<=122) ) ) {
			printf("[ERROR] - Invalid nickname characters\n");
			print_help();
			close_error();
		}  
	}
	strncpy(cfg.nickname,nickname,CONF_NICKNAME_SIZE);
	cfg.msgNumber = 0;
	
	
	return cfg;
}
Node load_server_config(char *string) {
	int found = MYFALSE;
	int i = 0;
	int size = strlen(string);
	
	Node server = new_Node();
	
	if ( strcmp(string,"localhost") == 0) { 
		strcpy(server.host_addr,"localhost");
		return server;
	}
	// Extract IP Address
	while ( found != MYTRUE && i < size ) {
		if ( string[i] == ':' ) {
			found = MYTRUE;
			break;
		}
		server.host_addr[i] = string[i]; 
		i++;
	}
	
	// If there is no ':' char --> invalid ip addr
	if ( ( found == MYFALSE && i == size ) || validateIPAddress(server.host_addr) == MYFALSE ) { print_help(); close_error(); }
	
	
	// Extract port
	i = i+1;
	int j = 0; 
	while ( i < size ) {
		server.host_port[j] = string[i];
		i++;j++;
	}
	
	if ( validatePortNumber(server.host_port) == MYFALSE ) {
		printf("El puerto indicado [ %s ] no es un puerto valido\n",server.host_port);
		close_error();
	}
	
	return server;
}

void print_help() {
	printf("\n[HELP DOC]\n");
	printf("Usage: 2 args, 1st ip:port, 2nd nickname\n");
	printf("\tIP: A valid IPv4 address.");
	printf("\tPort: A valid port in range port ( 1024 - 65535 )\n");
	printf("\tNickname: Only chars from A-Z or a-z and min length: 3 chars, max %d chars\n",CONF_NICKNAME_SIZE);
}


// CONSTRUCTORS
Config new_Config() {
	Config cfg;
	cfg.msgNumber = 0;
	bzero(cfg.nickname,CONF_NICKNAME_SIZE);
	return cfg;
}
Node new_Node() {
	Node node;
	bzero(node.host_addr,NODE_HOSTADDR_SIZE);
	bzero(node.host_port,NODE_HOSTPORT_SIZE);
	return node;
}
