package Utils;

public class GFunctions {
	
	public static long ID = 0;
	
	public static String cleanNullString(String inputString) {
		String outString = "";
		if ( inputString == null ) return outString; 
		if ( inputString.equals("null") ) return outString;
		if ( inputString.equals("(null)") ) return outString;
		return inputString.trim();
	}

	public static void writeToScreen(String message) {
		System.out.println(GFunctions.ID+" - "+message);
		GFunctions.ID++;
	}
}
