import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import Network.Frame;
import Utils.GFunctions;

public class ServerScreen {

	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(10000); 
			System.out.println("[INFO] Screen server started on port 10000");
			System.out.println("[INFO] Waitting for clients");
			while (true) {
				try {
					Socket client = serverSocket.accept();
					ObjectInputStream inputStream = new ObjectInputStream(client.getInputStream());
					ObjectOutputStream outputStream = new ObjectOutputStream(client.getOutputStream());
					
					Frame frameRX = (Frame) inputStream.readObject();
					GFunctions.writeToScreen("[FRAME] From: "+frameRX.nodename+" - "+frameRX.value);
					
					frameRX.ack = true;
					outputStream.writeObject(frameRX);
					
					inputStream.close();
					outputStream.close();
					client.close();
				} catch (Exception e) {
					e.printStackTrace();
				}	
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
