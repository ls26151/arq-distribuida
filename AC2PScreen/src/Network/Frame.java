package Network;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Frame implements Serializable {
	public String nodename;
	public String token;
	public String value;
	public boolean ack;
	public int type;
	
	public static final int FRAME_TOKEN_SND = 1000;
	public static final int FRAME_TOKEN_ACK = 1001;
	public static final int FRAME_TOKEN_ERR = 1002;
	
	
	public Frame(String nodename, String token, String value, int type, boolean ack) {
		this.nodename = nodename;
		this.token = token;
		this.value = value;
		this.ack = ack;
		this.type = type;
	}
	
}
