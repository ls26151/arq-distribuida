package Lamport;

import java.util.concurrent.locks.Lock;

import javax.swing.JLabel;

import Utils.GFunctions;
import Utils.Simbols;

public class LamportMutex {
	
	private int myID;
	private int tick;
	private String[][] matrix;
	private int[] requestQueue;
	private int size;
	private JLabel[][] labels;
	private JLabel[][] queueLabel;
	private Lock lock;
	private DirectClock clock;
	
	public static final int REQUEST = 10;
	public static final int RELEASE = 20;
	
	public LamportMutex(int myID,String[][] matr, JLabel[][] matrixLabels, JLabel[][] queueLabel, Lock lock) {
		this.myID = myID;
		this.tick = 1;
		this.matrix = matr;
		this.size = matrix[0].length;
		this.requestQueue = new int[size];
		this.queueLabel = queueLabel;
		this.cleanQueue(this.requestQueue);
		this.labels = matrixLabels;
		this.queueLabel[1][0].setText("Data:");
		this.lock = lock;
		this.clock = new DirectClock(new int[size], this.myID);
	}
	
	
	public void requestCS() {
		this.clock.tick();
		this.requestQueue[this.myID] = clock.getValue(this.myID);
		
		this.queueLabel[0][this.myID].setText(""+clock.getValue(this.myID));
		this.queueLabel[1][0].setText("RQ: "+this.requestQueue[this.myID]);
		this.queueLabel[1][1].setText("DC: "+clock.getValue(this.myID));

		// Esperamos a que los otros nodos hayan procesado todos los mensajes anteriores que les hemos enviado!
		// Y mientras vamos procesando tambien los nuestros para evitar un Deadlock
		this.readMessages();
		
		// Ahora que ya todos nuestros mensajes anteriores han sido procesados, procedemos a solicitar la CS
		GFunctions.writeToLog("TH"+this.myID+" Seccion critica solicitada");
		
		this.broadcastMessage(LamportMutex.REQUEST, this.requestQueue[this.myID]);
		while ( !this.okayCS() ) {
			this.myWait();
		}
	}

	public void releaseCS() {
		this.requestQueue[this.myID] = Simbols.Infinity;
		this.queueLabel[0][this.myID].setText(""+this.requestQueue[this.myID]);
		this.broadcastMessage(LamportMutex.RELEASE, Simbols.Infinity);
	}
	
	public boolean okayCS() {
		// En vez de hacer la lectura en el MyWait, ya que puede que tengamos un mensaje nuevo
		this.readMessages();
		for ( int i=0; i<this.size;i++ ) {
			String directClockData = "";
			String requestQueueData = "";
			for (int j=0; j<this.size; j++) { 
				directClockData += clock.getValue(j)+" ";
				requestQueueData += requestQueue[j]+" ";
			}
			GFunctions.writeToLog("TH"+this.myID+" DirectClock["+directClockData+"] RequestQueue["+requestQueueData+"]");
			if (this.isGreater(this.requestQueue[this.myID], this.requestQueue[i], this.myID, i)) return false;
		}
		return true;
	}
	
	private boolean isGreater(int entry1,int entry2,int pos1,int pos2) {
		if ( entry2 == Simbols.Infinity ) { GFunctions.writeToLog("TH"+this.myID+" Resultado de isGreater(e1:"+entry1+",e2:"+entry2+",p1:"+pos1+",p2:"+pos2+") == Infinity"); return false; }
		boolean retorno = ( (entry1 > entry2) || ((entry1 == entry2) && (pos1 > pos2)) );
		GFunctions.writeToLog("TH"+this.myID+" Resultado de isGreater(e1:"+entry1+",e2:"+entry2+",p1:"+pos1+",p2:"+pos2+") == "+retorno);
		return retorno;
	}
	
	public void myWait() {
		GFunctions.sleep(250);
	}
	

	public void readMessages() {
		boolean mensajesSinProcesar = false;
		for (int i=0; i<this.size; i++) {
				
				this.handleMessage(this.matrix[i][this.myID],i);	
				// Vamos a hacer que no se puedan escapar mensajes, por lo tanto vamos a hacer como si se produjera un ACK
				if ( this.messageSinProcesar(i) == true ) { mensajesSinProcesar = true; }
				if ( ((i+1) == size ) && (mensajesSinProcesar == true) ) {
//					GFunctions.writeToScreen("TH"+this.myID+" Hay mensajes sin procesar");
					// Como en esta vuelta hemos encontrado mensajes sin procesar, volvemos a iterar
					i=0;
					mensajesSinProcesar = false;
					GFunctions.sleep(100);
				}
				this.queueLabel[1][0].setText("RQ: "+this.requestQueue[this.myID]);
				this.queueLabel[1][1].setText("DC: "+clock.getValue(this.myID));
		}
		
		
	}
	private boolean messageSinProcesar(int pos) {
		int index = this.matrix[this.myID][pos].indexOf("@");
		if ( index != -1 ) {
//			GFunctions.writeToScreen("Mensaje sin procesar en ["+this.myID+"]["+pos+"] "+index);
			return true;
		}
		return false;
	}
	private void handleMessage(String message, int sender) {
//		GFunctions.writeToScreen("TH"+this.myID+" Entro en handle Message ");
		if ( message.indexOf("@") != -1 ) {
			// Nos han enviado un mensaje!
			GFunctions.writeToLog("TH"+this.myID+" Mensaje recibido "+message+" sender: TH"+sender);
			String[] values = message.split("@");
			int typeMessage = Integer.parseInt(values[0]);
			int timestamp = Integer.parseInt(values[1]);
			clock.receiveAction(sender, timestamp);
			if ( typeMessage == LamportMutex.REQUEST || typeMessage == LamportMutex.RELEASE ) {
				// 	Cargamos el timestamp del emisor en nuestra cola
				this.requestQueue[sender] = timestamp;
				this.queueLabel[0][sender].setText(""+timestamp);
				if ( typeMessage == LamportMutex.RELEASE ) {
					requestQueue[sender] = Simbols.Infinity;
				}
//				GFunctions.writeToScreen("TH"+this.myID+" Escribo en mi priority Queue["+sender+"] el valor "+timestamp);

			}
			
			// Ahora vamos a resetear el campo en la matriz de comunicaciones para marcarlo como leido
			this.lock.lock();
				this.matrix[sender][this.myID] = "0";	
			this.lock.unlock();
			this.labels[sender][this.myID].setText("0");
			String hola = "";			
//			GFunctions.writeToScreen("TH"+this.myID+" Reseteo el valor de ["+sender+"]["+this.myID+"]");
		}
	}
	private void broadcastMessage(int type, int value) {
		switch(type) {
			case LamportMutex.REQUEST:
				for (int i=0; i<this.size;i++) {
					if ( i != this.myID ){
						this.lock.lock();
							this.matrix[this.myID][i] = LamportMutex.REQUEST+"@"+value;
						this.lock.unlock();
							this.labels[this.myID][i].setText(this.matrix[this.myID][i]); 
							String hola = "";
						
					}
				}
			break;
			case LamportMutex.RELEASE:
				for (int i=0; i<this.size;i++) {
					if ( i != this.myID ) {
						this.lock.lock();
							this.matrix[this.myID][i] = LamportMutex.RELEASE+"@"+Simbols.Infinity;
						this.lock.unlock();	
							this.labels[this.myID][i].setText(this.matrix[this.myID][i]);
							String hola ="";
					}
				}				
			break;
		}
	}
	
	public void cleanQueue(int[] priorityQueue) {
		for (int i=0;i<size;i++) {
			this.requestQueue[i] = Simbols.Infinity;
			this.queueLabel[0][i].setText(""+Simbols.Infinity);
		}
	}
	
	public void reset() {
		this.clock = new DirectClock(new int[this.size], this.myID);
	}
	
}
