package Lamport;

import Utils.GFunctions;

public class DirectClock {
	
	public int clock[];
	private int myID;
	public DirectClock (int[] queue, int id) {
		this.myID = id;
		clock = queue;
	}

	public int getValue(int i) {
		return clock[i];
	}
	
	private void cleanClock() {
		for (int i=0; i<clock.length; i++) {
			clock[i] = 0;
		}
		clock[this.myID] = 1;
	}
	public void tick() {
		clock[this.myID]++;
	}
	public void receiveAction(int sender, int value) {
		clock[sender] = GFunctions.max(clock[sender], value);
		clock[myID] = GFunctions.max(clock[myID], value) + 1;
	}
}
