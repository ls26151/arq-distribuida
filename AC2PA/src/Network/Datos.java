package Network;

public class Datos {
	
	private static Datos instance;
	private String token;
	public static String[][] comMatrix;
	
	
	
	public final static String PLAMPOR_HOST = "localhost";
	public final static String PRICARD_HOST = "localhost";
	public final static String PSCREEN_HOST = "localhost";
	public final static int PSCREEN_PORT = 10000;
	public final static int PLAMPOR_PORT = 10001;
	public final static int PRICARD_PORT = 10002;
	
	private Datos() {
		token = "";
	}
	
	public static Datos getInstance() {
		if ( instance == null ) {
			instance = new Datos();
		}
		return instance;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean hasToken() {
		if ( token.equals("") ) {
			return false;
		}
		return true;
	}
	
}
