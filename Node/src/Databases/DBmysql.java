package Databases;

import java.io.FileReader;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import Config.GFunctions;

public class DBmysql {

	public static final int DEFAULT_PORT = 3306;
	public static final String DB_CONFIG_FILE = "db.properties";
	public static DBmysql instance;
	
	private String db_host;
	private String db_name;
	private String db_pass;
	private String db_user;
	private String db_port;
	private final String db_driver = "com.mysql.jdbc.Driver";
	private final String db_url = "jdbc:mysql://";
	private java.sql.Connection db_link;
	
	private DBmysql(String dbase_host, String dbase_user, String dbase_pass, String dbase_name, String dbase_port) {
		// With port
		this.db_host = dbase_host;
		this.db_user = dbase_user;
		this.db_pass = dbase_pass;
		this.db_name = dbase_name;
		this.db_port = dbase_port;
	}
	private DBmysql(String dbase_host, String dbase_user, String dbase_pass, String dbase_name) {
		// Without port --> Using default port
		this.db_host = dbase_host;
		this.db_user = dbase_user;
		this.db_pass = dbase_pass;
		this.db_name = dbase_name;
		this.db_port = String.valueOf(DBmysql.DEFAULT_PORT);
	}
	
	public static DBmysql getInstance() {
		if ( instance == null ) {
			Properties props = DBmysql.getProperties();
			String host = GFunctions.cleanNullString( props.getProperty("db_host"));
			String port = GFunctions.cleanNullString( props.getProperty("db_port"));
			String name = GFunctions.cleanNullString( props.getProperty("db_name"));
			String user = GFunctions.cleanNullString( props.getProperty("db_user"));
			String pass = GFunctions.cleanNullString( props.getProperty("db_pass"));
			if ( !host.equals("") && !name.equals("") && !user.equals("") && !pass.equals("") ) {
				if ( port.equals("") ) {
					instance = new DBmysql(host,user,pass,name);
				} else { 
					instance = new DBmysql(host,user,pass,name,port);
				}
			} else {
				System.out.println("ERROR: Failed database config load");
				System.exit(-1);
			}
			
		}
		return instance;
	}
	
	
	public boolean connect() {
		boolean status = false;
		String connectionURL = this.db_url + this.db_host + ":" + this.db_port + "/";
		if ( this.db_link == null ) {
			try {
				this.db_link = java.sql.DriverManager.getConnection(connectionURL,this.db_user,this.db_pass);
			} catch (Exception e) {
				status = false;
			}
		} else {
			status = true;
		}
		return status;
	}
	
	public java.sql.ResultSet execReadableQuery(String query) {
		java.sql.ResultSet resultset = null;
		try {
			if ( this.connect() ) {
				Statement stmt = this.db_link.createStatement();
				resultset = stmt.executeQuery(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultset = null;
		}
		return resultset;
	}
	
	public int execWritableQuery(String query) {
		int rowsAffected = 0;
		try {
			if ( this.connect() ) {
				Statement stmt = this.db_link.createStatement();
				rowsAffected = stmt.executeUpdate(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			rowsAffected = 0;
		}
		return rowsAffected;
	}

	public void openTransaction() throws SQLException {
		this.db_link.setAutoCommit(false);
	}
	
	public void closeTransaction(boolean commit) throws SQLException {
		if ( commit ) {
			this.db_link.commit();
		} else {
			this.db_link.rollback();
		}
		
		
	}
	
	private static Properties getProperties() {
		Properties properties;
		try{ 
			FileReader reader = new FileReader(DBmysql.DB_CONFIG_FILE);
			properties = new Properties();
			properties.load(reader);
		} catch (Exception e) {
			properties = null;
		}
		return properties;
	}
	
	
}
