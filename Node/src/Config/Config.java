package Config;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Data.Datos;
import Network.DistribNode;
import Network.NodeList;

public class Config {

	public static final String CONFIGFILE = "config.properties";
	public static Config instance = null;
	

	private String	sv_name;
	private String 	sv_host;
	private int 	sv_port;
	private String 	ws_nodelist;
	private boolean	mode_write;
	
	private Config() {}
	public static Config getInstance() {
		if ( instance == null) {
			instance = new Config();
		}
		return instance;
	}
	
	public boolean loadConfig() {
		boolean loadOK = false;
		Properties props = this.getProperties();
		if ( props != null) {
			try {
				this.sv_name = GFunctions.cleanNullString(props.getProperty("sv_name"));
				this.ws_nodelist = GFunctions.cleanNullString(props.getProperty("ws_nodelist"));
				
				if ( this.sv_name.equals("") || this.ws_nodelist.equals("") ) {
					throw new IOException("Failed loading data from file: "+Config.CONFIGFILE);
				}
				System.out.println("Getting config and nodes from WService ...");
				
				if ( loadConfigFromWService() ) {
					
				} else {
					throw new IOException("ERROR: Problems loading config from Webservice ...");
				}
			
				loadOK = true;
				//System.out.println("Loaded cfg [Name: "+this.sv_name+"] - [Host: "+this.sv_host+"] - [Port: "+this.sv_port+"] - [WriteMode: "+this.mode_write+"]");
			} catch (Exception e) {
				loadOK = false;
			}
		}
		return loadOK;
	}
	
	public Properties getProperties() {
		Properties properties;
		try{ 
			FileReader reader = new FileReader(Config.CONFIGFILE);
			properties = new Properties();
			properties.load(reader);
			System.out.println("*** ARQ.DISTR soft v1 ***");
		} catch (Exception e) {
			properties = null;
		}
		return properties;
	}
	
	public String getName() {
		return sv_name;
	}
	public void setName(String sv_name) {
		this.sv_name = sv_name;
	}
	public String getHost() {
		return sv_host;
	}
	public void setHost(String sv_host) {
		this.sv_host = sv_host;
	}
	public int getPort() {
		return sv_port;
	}
	public void setPort(int sv_port) {
		this.sv_port = sv_port;
	}
	public boolean isWritableMode() {
		return mode_write;
	}
	public void setWritableMode(boolean mode_write) {
		this.mode_write = mode_write;
	}
	
	public boolean loadConfigFromWService() {
		boolean loadOK = true;
		
		try{
			URL url = new URL(this.ws_nodelist);	
			HttpURLConnection connection =(HttpURLConnection) url.openConnection();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line = "";
			String json = "";
			
			while ((line = reader.readLine()) != null){
				json +=line;
			}
			
			Gson gson = new GsonBuilder().create();
			JsonObject object = gson.fromJson(json, JsonObject.class);
			JsonArray data = object.get("data").getAsJsonArray();
			NodeList nodelist = NodeList.getInstance();
			/* Ahora vamos a realizar 2 tareas
			 * 1 - Buscar nuestra configuracion
			 * 2 - Los que no seamos nosotros, los aņadiremos al listado de nodos. 
			 */
			
	    	for (JsonElement element: data){
	    		String host = element.getAsJsonObject().get("sv_host").getAsString();
	    		String port = element.getAsJsonObject().get("sv_port").getAsString();
	    		String name = element.getAsJsonObject().get("sv_name").getAsString();
	    		String mode = element.getAsJsonObject().get("sv_mode").getAsString();
	    		boolean writeMode = false;
	    		if ( mode.equals("write") ) writeMode = true; else writeMode = false;
	    		
	    		if ( this.sv_name.equalsIgnoreCase(name) ) {
	    			// Es nuestra configuracion!
	    			this.sv_host = host;
	    			this.sv_port = Integer.parseInt(port);
	    			this.mode_write = writeMode;
	    			if ( this.mode_write ) {
	    				String token = GFunctions.cleanNullString(element.getAsJsonObject().get("sv_token").getAsString());
	    				(Datos.getInstance()).setToken(token);
	    			}
	    		} else {
	    			// Es otro nodo!
	    			DistribNode dn = new DistribNode(name,host,Integer.parseInt(port));
	    			nodelist.addNode(dn);
	    		}
			}
	    	System.out.println("Loaded config from WS [Host: "+this.sv_host+"] [Port: "+this.sv_port+"] [Mode: "+this.mode_write+"] [Token: "+Datos.getInstance().getToken()+"]  <> and "+nodelist.getArrayList().size()+" nodes added to list");
			loadOK = true;
		} catch (Exception e) {
			loadOK = false;
			e.printStackTrace();
		}
		
		return loadOK;
	}

}
