package Config;
import java.net.InetAddress;
import java.util.Date;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

public class GFunctions {
	
	public static final String SERVER_NTP = "hora.roa.es";
	
	public static String cleanNullString(String inputString) {
		String outString = "";
		if ( inputString == null ) return outString; 
		if ( inputString.equals("null") ) return outString;
		if ( inputString.equals("(null)") ) return outString;
		return inputString.trim();
	}

	public static long getTStampFromNTP() {
		long retTime = -1;
		try {
			NTPUDPClient timeClient = new NTPUDPClient();
			InetAddress inetAddress = InetAddress.getByName(GFunctions.SERVER_NTP);
	        TimeInfo timeInfo = timeClient.getTime(inetAddress);
	        Date time = new Date(timeInfo.getReturnTime());
	        retTime = time.getTime();
		} catch (Exception e) {
			retTime = -1;
		}
        return retTime;
	}
	public static void writeToScreen(String message) {
		Config cfg = Config.getInstance();
		System.out.println(cfg.getName()+" - "+message);
	}
}
