import Config.Config;
import Data.Datos;
import Network.Server;

public class Main {

	public static void main(String[] args) {
		Config cfg = Config.getInstance();
		Datos data = Datos.getInstance();
		Server server;
		if ( cfg.loadConfig() ) {
			// Start multiserver
			server = new Server( cfg.getPort() );
		} else {
			System.out.println("ERROR: Can't load config file");
		}
	}

}
