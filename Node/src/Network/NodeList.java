package Network;
import java.util.ArrayList;

public class NodeList {

	private static NodeList instance = null;
	
	private ArrayList<DistribNode> nodes;
	
	private NodeList() {
		nodes = new ArrayList<DistribNode>();
	}
	
	public static NodeList getInstance() {
		if ( instance == null ) {
			instance = new NodeList();
		}
		return instance;
	}
	public ArrayList<DistribNode> getArrayList() {
		return this.nodes;
	}
	public void addNode(DistribNode node) {
		synchronized (this) {
			this.nodes.add(node);
		}
	}
	
	public void setList(ArrayList<DistribNode> nodeList) {
		synchronized (this) {
			this.nodes = nodeList;
		}
	}
	public void updateTimeStampOfNode(String nodename,long tstamp) {
		
		for ( int i= 0; i<this.nodes.size(); i++) {
			DistribNode dn = this.nodes.get(i);
			if ( dn.getNode_name().equalsIgnoreCase(nodename) ) {
				dn.setNode_lastTimeStamp(tstamp);
				break;
			}
		}
		
	}
}
