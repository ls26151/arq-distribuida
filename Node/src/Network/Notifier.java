package Network;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

import Config.GFunctions;
import Data.Datos;

public class Notifier extends Thread {
	private Frame frameTX;
	
	public Notifier(Frame frame) {
		this.frameTX = frame;
	}

	@Override
	public void run() {
		NodeList nList = NodeList.getInstance();
		Datos data = Datos.getInstance();
		int size = nList.getArrayList().size();
		
		Random randGen = new Random();
		boolean sendToken = false;
		boolean sendedToken = false;
		int rand = 0;
		
		if ( !frameTX.token.equals("") ) { 
			sendToken = true;
			frameTX.token = "";
			rand = randGen.nextInt(size);
		}
		//GFunctions.writeToScreen("THREAD: [START] Notifier with value ["+frameTX.value+"] ");
		for ( int i = 0; i<size; i++) {
			Socket socket;
			DistribNode dn = nList.getArrayList().get(i);
			try {
				// Si segun nuestro listado, la trama que le vamos a notificar es 
				// mayor a la ultima trama que le enviamos --> entonces le actualizamos
				if ( this.frameTX.timestamp > dn.getNode_lastTimeStamp() ) {
					// Abrimos conexion con el nodo
					
					socket = new Socket(dn.getNode_host(),dn.getNode_port());
					ObjectOutputStream oStream = new ObjectOutputStream(socket.getOutputStream());
				
					if ( sendToken && rand == i ) {
						Frame frameWithToken = new Frame(frameTX.nodename,data.getToken(),frameTX.timestamp,frameTX.value);
						oStream.writeObject(frameWithToken);
						data.setToken(""); 
						sendedToken = true;
						GFunctions.writeToScreen("INFO: TOKEN SENDED TO "+dn.getNode_name());
					} else {
						//GFunctions.writeToScreen("Voy a enviar trama sin token");
						Frame noTokenFrame = new Frame(frameTX.nodename,"",frameTX.timestamp,frameTX.value);
						oStream.writeObject(noTokenFrame);
					}
				
					// Cerramos conexion con el nodo
					oStream.close();
					socket.close();
					
					dn.setNode_lastTimeStamp(this.frameTX.timestamp);
				}
			} catch (Exception e) {
				GFunctions.writeToScreen("ERROR: No se pudo enviar update ["+frameTX.value+"] a el nodo "+dn.getNode_name());
			}
			if ( i+1 == size ) {
				if ( sendToken == true && sendedToken == false ) {
					i = -1; 
					this.frameTX.timestamp  = this.frameTX.timestamp + 1;
					rand = randGen.nextInt(size);
				}
			}
		}
		//GFunctions.writeToScreen("THREAD: [END] Notifier with value ["+frameTX.value+"] ended");
	}
	
	
	
	
}
