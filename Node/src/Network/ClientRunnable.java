package Network;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import Config.Config;
import Config.GFunctions;
import Data.Datos;

public class ClientRunnable extends Thread{
	
	public static final int MAX_OPERATIONS = 10;
	protected Socket clientSocket = null;
	protected ObjectInputStream inputStream;
	protected ObjectOutputStream outputStream;
	
	public ClientRunnable(Socket cSocket) {
		this.clientSocket = cSocket;
	}

	@Override
	public void run() {
		Config cfg = Config.getInstance();
		Datos data = Datos.getInstance();
		NodeList nList = NodeList.getInstance();
		try {
			// Abrimos canal
			this.inputStream  = new ObjectInputStream(clientSocket.getInputStream());
	
			// Recibimos 1 trama
			Frame frameRX = (Frame) inputStream.readObject();
				
			// Cerramos conexiones
			inputStream.close();
			clientSocket.close();
			
			if ( frameRX.timestamp > data.getLastTStamp() || !frameRX.token.equals("") ) {
				//GFunctions.writeToScreen(" -- Timestamp [R: "+frameRX.timestamp+" - M: "+data.getLastTStamp()+" ]  --");
				GFunctions.writeToScreen("INFO: Updated value from external frame ["+data.getCurrentValue()+"] to ["+frameRX.value+"] ");
				data.updateCurrentValue(frameRX.value);
				
				data.setLastTStamp(frameRX.timestamp);
				nList.updateTimeStampOfNode(frameRX.nodename, frameRX.timestamp);

				if ( !frameRX.token.equals("") ) {
					GFunctions.writeToScreen("INFO: TOKEN RECEIVED FROM "+frameRX.nodename+" !");
					data.setToken(frameRX.token);
					this.grayskull();
				} else {
					Frame frameTX = new Frame(cfg.getName(), "" ,frameRX.timestamp,frameRX.value);
					Notifier notifier = new Notifier(frameTX);
					notifier.start();
				}
			
			} else {
				GFunctions.writeToScreen("INFO: Discarded frame from "+frameRX.nodename+" (lower or equal timestamp) [RX:"+frameRX.timestamp+" MEM:"+data.getLastTStamp()+"]");
			}
		} catch (Exception e) {
			System.out.println("ERROR: Problems with socket connection, closing thread");
			e.printStackTrace();
		}
	}
	
	/**
	 * Actualiza los valores!
	 */
	public void grayskull() {
		Datos data = Datos.getInstance();
		Config cfg = Config.getInstance();
		int tmpData = 0;
		Frame frameTX;
		Notifier notifier;
		for ( int i = 0; i<MAX_OPERATIONS; i++) {
			synchronized (this){
				long tstamp = 0;
				tmpData = data.getCurrentValue();
				GFunctions.writeToScreen("INFO: Actual value ["+tmpData+"]");
				if ( cfg.isWritableMode() ) {
					tmpData = tmpData + 1;		
					data.updateCurrentValue(tmpData); 
					tstamp = GFunctions.getTStampFromNTP();
					data.setLastTStamp(tstamp);
					GFunctions.writeToScreen("INFO: Por el poder de Grayskull, yo incremento el valor! [Value: "+data.getCurrentValue()+"] ");
					if ( i+1 < MAX_OPERATIONS ) {
						
						frameTX = new Frame( cfg.getName(), "" , data.getLastTStamp(), data.getCurrentValue() );
						notifier = new Notifier(frameTX);
						notifier.start();
					}
				} 
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}
		frameTX = new Frame( cfg.getName(), data.getToken(), data.getLastTStamp(), data.getCurrentValue() );
		notifier = new Notifier(frameTX);
		notifier.start();
	}
}
