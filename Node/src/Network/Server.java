package Network;

import java.net.ServerSocket;
import java.net.Socket;

import Config.GFunctions;
import Data.Datos;

public class Server implements Runnable {
	
	private boolean stopped = true;
	private ServerSocket serverSocket = null;
	private int server_port;
	
	public Server(int port) {
		
		this.server_port = port;
		
		Datos data = Datos.getInstance();
		if ( data.hasToken() ) {
			Thread th = new Thread() {
				public void run() {
					// Si tenemos el token, lanzamos el poder de Grayskull
					ClientRunnable cr = new ClientRunnable(null);
					cr.grayskull();
				}
			};
			th.start();
		}
		
		this.startServer();

	}
	
	public void startServer() {
		try {
			serverSocket = new ServerSocket(this.server_port);
			this.stopped = false;
			GFunctions.writeToScreen("SERVER: Starting server on port ["+this.server_port+"]");
		} catch (Exception e) {
			GFunctions.writeToScreen("ERROR: Problemas al iniciar el servidor");
		}
		
		while ( !this.isStopped() ) {
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
			} catch (Exception e) {
				GFunctions.writeToScreen("ERROR: Problems accepting connection from node");	
			}
			if ( clientSocket != null ) {
				ClientRunnable clientThread = new ClientRunnable(clientSocket);
				clientThread.start();
			}
		}
			
			
	}
	
	public void stopServer() {
		this.stopped = true;
		try {
			
		} catch(Exception e) {
			
		}
		
	}
	
	public boolean isStopped() {
		return this.stopped;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
