package Network;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Frame implements Serializable {
	public String nodename;
	public String token;
	public long timestamp;
	public int value;
	
	public Frame(String nodename, String token, long timestamp, int value) {
		this.nodename = nodename;
		this.token = token;
		this.timestamp = timestamp;
		this.value = value;
	}
	
}
