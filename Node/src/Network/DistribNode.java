package Network;

public class DistribNode {
	private String node_name;
	private String node_host;
	private int node_port;
	private long node_lastTimeStamp;
	
	public DistribNode(String node_name, String node_host, int node_port) {
		this.node_name = node_name;
		this.node_host = node_host;
		this.node_port = node_port;
		this.node_lastTimeStamp = 0;
	}
	
	protected String getNode_name() {
		return node_name;
	}
	protected void setNode_name(String node_name) {
		this.node_name = node_name;
	}
	protected String getNode_host() {
		return node_host;
	}
	protected void setNode_host(String node_host) {
		this.node_host = node_host;
	}
	protected int getNode_port() {
		return node_port;
	}
	protected void setNode_port(int node_port) {
		this.node_port = node_port;
	}
	protected synchronized long getNode_lastTimeStamp() {
		return node_lastTimeStamp;
	}
	protected synchronized void setNode_lastTimeStamp(long node_lastTimeStamp) {
		this.node_lastTimeStamp = node_lastTimeStamp;
	}
	
}
