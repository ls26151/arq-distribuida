package Data;

public class Datos {
	
	private static Datos instance;
	
	private int 	shared_value;
	private String 	shared_token;
	private long 	last_timestamp;
	
	
	private Datos() {
		this.shared_value = 0;
		this.shared_token = "";
		this.last_timestamp = 0;
		
	}
	
	public static Datos getInstance() {
		if ( instance == null ) {
			instance = new Datos();
		}
		return instance;
	}
	
	public synchronized int getCurrentValue() {
		return this.shared_value;
	}

	public synchronized void updateCurrentValue(int value) {
		this.shared_value = value;
	}

	public String getToken() {
		return shared_token;
	}
	public void setToken(String shared_token) {
		this.shared_token = shared_token;
	}
	
	public synchronized long getLastTStamp() {
		return this.last_timestamp;
	}
	public synchronized void setLastTStamp(long tstamp) {
		this.last_timestamp = tstamp;
	}
	public boolean hasToken() {
		if ( this.getToken().equals("") ) {
			return false;
		}
		return true;
	}
}
