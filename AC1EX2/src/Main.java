import java.util.Calendar;

import sorter.Sorter;

public class Main {

	public static void main(String[] args) {
		int MAX_SIZE = 10000;
		int valueToFind = 4;
			
		int resultIndex = 0;
		int array1[] = new int[MAX_SIZE];
		int array2[] = new int[MAX_SIZE];
		for ( int i = 0; i<MAX_SIZE; i++) {
			array1[i] = (int) (Math.random()*2000);
			array2[i] = array1[i];
			System.out.print(array1[i]+",");
		}
		System.out.println("");
		System.out.println("");
		Sorter.orderSequential(array1);
//		for ( int i = 0; i<array1.length; i++) {
//			System.out.print("["+array1[i]+"] ");
//		}		

		long init_tstamp1 = Calendar.getInstance().getTimeInMillis();
		Sorter.orderMerge(array1);
		long execTimeSeq = Calendar.getInstance().getTimeInMillis() - init_tstamp1;
		long init_tstamp2 = 0;
		try {
			init_tstamp2 = Calendar.getInstance().getTimeInMillis();	
			Sorter.orderMergeThreaded(array2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long execTimeThr = Calendar.getInstance().getTimeInMillis() - init_tstamp2;
		
		for ( int i = 0; i<array1.length; i++) {
			System.out.print("["+array1[i]+"] ");
		}	
		System.out.println("");
		for ( int i = 0; i<array2.length; i++) {
			System.out.print("["+array2[i]+"] ");
		}	
		System.out.println("");
		
		System.out.println("SEQ: "+execTimeSeq+" ms");
		System.out.println("THR: "+execTimeThr+" ms");
//		
//		LinkedList<Integer> array = new LinkedList<Integer>();
//		for ( int i = 0 ; i<MAX_SIZE; i++) {
//			array.add(i);
//		}
//
//		resultIndex = Finder.cercaParallelaDual(valueToFind, array);
//		if ( resultIndex != -1 ) {
//			System.out.println("Encontrado en la posicion: "+resultIndex);
//		} else { 
//			System.out.println("No se ha encontrado el valor indicado");
//		}

//		
//		int MAX_THREADS = 10;
//
//			int miArray[] = new int[MAX_SIZE];
//			int randomValueToFind = 50; 
//			int numThreads = 10;
////			while (randomValueToFind == 0 ) { randomValueToFind= 0 + (int)(Math.random()*MAX_SIZE); } 
////			while (numThreads == 0 ) { numThreads = (int) (Math.random()*MAX_THREADS); }
//			
//			for (int i=0; i<MAX_SIZE;i++) { miArray[i] = i; }
//			
//			resultIndex = Finder.cercaParallela(randomValueToFind, miArray, numThreads);
//			
//			if ( resultIndex != -1 ) {
//				System.out.println("[MAX_SIZE "+MAX_SIZE+" | V: "+randomValueToFind+" | THS: "+numThreads+"] Encontrado en la posicion: "+resultIndex);
//			} else { 
//				System.out.println("[MAX_SIZE "+MAX_SIZE+" | V: "+randomValueToFind+" | THS: "+numThreads+"] No se ha encontrado el valor indicado ["+randomValueToFind+"]");
//			}
//		//}
//		
//		
		

	}	
}
