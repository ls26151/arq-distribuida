package finder;
/**
 * 
 * @author Guille Rodriguez 
 *
 */
public class FinderControl {
	
	private boolean found;
	private int 	index;
	
	public FinderControl() {
		this.found = false;
		this.index = -1;
	}
	
	public boolean isFound() {
		return this.found;
	}
	public void setFound(boolean b) {
		this.found = b;
	}
	
	public int getIndexFound( ) {
		return this.index;
	}
	public void setIndexFound(int idx ) {
		this.index = idx;
	}
}
