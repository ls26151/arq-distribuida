package finder;
/**
 * 
 * @author Guille Rodriguez 
 *
 */
public class FinderMultipleThread extends Thread{

	private int[] arrayIntegers;
	private FinderControl control;
	private int initPosition;
	private int stopPosition;
	private int valueToFind;
	private int id;
	
	public FinderMultipleThread(FinderControl ctrl, int[] array, int startPos, int stopPos, int valueToFind, int id) {
		this.arrayIntegers = array;
		this.control = ctrl;
		this.initPosition = startPos;
		this.stopPosition = stopPos;
		this.valueToFind = valueToFind;
		this.id = id;
	}
	
	@Override
	public void run() {
		
		for (int i=this.initPosition; i<this.stopPosition && this.control.isFound() == false; i++) {
			System.out.println("Thread"+id+": - Iteracion["+i+"] Es "+this.arrayIntegers[i]+"=="+this.valueToFind);
		
			this.initPosition = i;
			if ( this.arrayIntegers[i] == this.valueToFind ) {
				System.out.println("THREAD"+id+": Found!!! Saving index & notifying to other Threads");
				this.control.setFound(true);
				this.control.setIndexFound(i);
				break;
			}
		}
		//System.out.println("THREAD["+this.id+"]: Saliendo del Thread con SP:"+this.initPosition+" EP:"+this.stopPosition);
	}
	
	
	
}
