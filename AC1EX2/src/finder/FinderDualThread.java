package finder;
import java.util.LinkedList;
/**
 * 
 * @author Guille Rodriguez 
 *
 */
public class FinderDualThread extends Thread{
	
	private boolean incrementalSearch = true;
	private LinkedList<Integer> array;
	private int startPosition;
	private int finalPosition;
	private int valueToFind;
	private FinderControl control;
		
	public FinderDualThread(FinderControl ctrl, int startPosition, int finalPosition,LinkedList<Integer> array,boolean incremental,int valueToFind){
		this.startPosition = startPosition;
		this.finalPosition = finalPosition;
		this.array = array;
		this.incrementalSearch = incremental;
		this.valueToFind = valueToFind;
		this.control = ctrl;
	}

	@Override
	public void run() {
		
		if ( incrementalSearch ) {
			for(int i=startPosition; (i<finalPosition) && (control.isFound() == false) ;i++) {
				System.out.println("THREAD - INC Count "+i);
				if ( this.valueToFind == this.array.get(i) ) {
					System.out.println("Soy normal y lo he encontrado");
					control.setFound(true);
					control.setIndexFound(i);
					break;
				}
			}
		} else {
			for (int i=(this.finalPosition-1); (i >= this.startPosition) && (control.isFound() == false) ; i--) {
				System.out.println("THREAD - DEC Count "+i);
				if ( this.valueToFind == this.array.get(i) ) {
					System.out.println("Soy reverse y lo he encontrado");
					control.setFound(true);
					control.setIndexFound(i);
					break;
				}
			}
		}
	}
}
