package finder;
import java.util.LinkedList;
/**
 * 
 * @author Guille Rodriguez 
 *
 */
public class Finder {
	
	public Finder () {}
	
	public static int cercaParallelaDual(int valueToFind, LinkedList<Integer> array) {
		
		int index = -1;
		int MAX_THREADS = 2;
		int arraySize = array.size();
		FinderControl control = new FinderControl();

		// Busqueda secuencial con dos Threads
		// Thread 0 --|> Inicia la busqueda de forma incremental
		// Thread 1 --|> Inicia la busqueda de forma decremental
		int SP = 0 * ( arraySize / MAX_THREADS );
		int EP = 1 * ( arraySize / MAX_THREADS ); 
		
		FinderDualThread th0 = new FinderDualThread(control,SP, arraySize, array, true, valueToFind);
		FinderDualThread th1 = new FinderDualThread(control,0, arraySize, array, false, valueToFind);
		
		// Starting threads
		th0.start(); th1.start();
		
		try {
			th0.join();
			th1.join();			
		} catch (Exception e) {}

		// Si lo han encontrado --> recuperamos el indice!
		if ( control.isFound() ) {
			index = control.getIndexFound();
		}
		
		return index;
	}
	public static int cercaParallela(int valueToFind, int[] array, int numThreads) {
		int index = -1;
		int MAX_THREADS = numThreads;
		FinderMultipleThread[] threads = new FinderMultipleThread[MAX_THREADS];
		FinderControl control = new FinderControl();
		
		int SP;
		int EP;
		int arraySize = array.length;
		for (int i=0; i<MAX_THREADS;i++) {
			SP = i * (arraySize / MAX_THREADS);
			if ( i+1 == MAX_THREADS ) EP=arraySize;
			else EP=(i+1)*(arraySize / MAX_THREADS);
			threads[i] = new FinderMultipleThread(control, array, SP, EP, valueToFind,i);
			threads[i].start();
		}
		
		for (int i=0; i<MAX_THREADS; i++) {
			try {
				threads[i].join();
			} catch (Exception e) {}
		}
		// Si lo han encontrado --> recuperamos el indice!
		if ( control.isFound() ) {
			index = control.getIndexFound();
		}
		
		return index;
	}
}
