package sorter;
/**
 * 
 * @author Guille Rodriguez 
 *
 */
public class SorterMergeThread extends Thread{
	private int array[];
	private int startIndex;
	private int finalIndex;
	
	public SorterMergeThread(int arr[], int start, int MAX ) {
		this.array = arr;
		this.startIndex = start;
		this.finalIndex = MAX;
	}
	
	@Override
	public void run() {
		int mySize = this.finalIndex - this.startIndex;
		if ( mySize > 2 ) {
			// Recursivo hacia abajo
			int SP1 = this.startIndex;
			int EP1 = this.startIndex + (mySize / 2);
			int SP2 = EP1;
			int EP2 = this.finalIndex;
			SorterMergeThread th1 = new SorterMergeThread(this.array, SP1, EP1);
			SorterMergeThread th2 = new SorterMergeThread(this.array, SP2, EP2);
			// Lanzamos los threads para seguir con el particionado
			th1.start();	//System.out.println("START TH1 - SP["+SP1+"] EP["+EP1+"]"); 
			th2.start();	//System.out.println("START TH2 - SP["+SP2+"] EP["+EP2+"]");
			try {
				th1.join();
				th2.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// Realizamos el merge
			this.mergeArrays(SP1, EP1, SP2, EP2);
		} else {
			this.merge();
		}
		// Salimos del thread hacia el thread.join del padre
	}
	
	protected void merge() {
		if ( this.array[this.startIndex] > this.array[(this.finalIndex-1)]) {
			this.swap(this.startIndex, (this.finalIndex-1));
		}		
	}

	protected void mergeArrays(int SP1, int EP1, int SP2, int EP2) {
		String dbg = "Iniciando Merge["+SP1+" - "+EP2+"]: ";
		int lIndex = SP1;
		int rIndex = SP2;
		int tmpIdx = 0;
		int retArray[] = new int[(EP2-SP1)];
		while ( lIndex < EP1 && rIndex < EP2 ) {
			if ( this.array[lIndex] < this.array[rIndex] ) {
				retArray[tmpIdx] = this.array[lIndex];
				lIndex++;
			} else {
				retArray[tmpIdx] = this.array[rIndex];
				rIndex++;
			}
			tmpIdx++;
		}
		while( rIndex < EP2 ) {
			retArray[tmpIdx] = this.array[rIndex];
			rIndex++; tmpIdx++;
		}
		while( lIndex < EP1 ) {
			retArray[tmpIdx] = this.array[lIndex];
			lIndex++; tmpIdx++;
		}
		// Sobreescribimos los valores del array bueno con los del array temporal
		tmpIdx = 0;
		for (int i = SP1; i<EP2; i++) {
			this.array[i] = retArray[tmpIdx];
			tmpIdx++;
		}
		
	}

	protected void swap (int ind1, int ind2) {
		int tmp = this.array[ind1];
		this.array[ind1] = this.array[ind2];
		this.array[ind2] = tmp;
	}
}
