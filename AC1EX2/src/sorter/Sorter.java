package sorter;
/**
 * 
 * @author Guille Rodriguez 
 *
 */
public class Sorter {

	public static void orderSequential(int array[]) {
		int size = array.length;
		for ( int i=0; i<size-1; i++) {
			for (int j=(i+1); j<size; j++) {
				if ( array[j] < array[i] ) {
					//Hacemos swap 
					int tmp = array[i];
					array[i] = array[j];
					array[j] = tmp;
				}
			}
		}
	}
	
	public static void orderMerge(int array[]) {
		SorterMergeSequential sms = new SorterMergeSequential(); 
		sms.sort(array);
	}
	
	public static void orderMergeThreaded(int array[]) throws InterruptedException {
		SorterMergeThread thread = new SorterMergeThread(array,0,array.length);
		thread.start();
		thread.join();
	}
}
