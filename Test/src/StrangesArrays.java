import java.util.Random;

public class StrangesArrays {
	int MAXcols = 10000;
	int MAXrows = 10000;
	Random rd;
	int[][] MATRIX = new int[MAXrows][MAXcols];
	int cantidad = 0;
	public StrangesArrays() {
		rd = new Random();
		this.initalizeMatrix();		
	}

	public void initalizeMatrix() {
		for (int i=0; i<MAXrows; i++) {
			for (int j=0; j<MAXcols; j++) {
				this.MATRIX[i][j] = this.getRandom();
			}
		}
	}
	public int getRandom() {
		return this.rd.nextInt(200);
	}

	public long recorreFilasColumnas(int value) {
		cantidad = 0;
		long tsStart = System.currentTimeMillis(); 
		long tsStop = 0;
		for (int i=0; i<MAXrows; i++) {
			for (int j=0; j<MAXcols; j++) {
				if ( this.MATRIX[i][j] == value ) { cantidad++; } 
			}
		}
		tsStop = System.currentTimeMillis();
		return (tsStop-tsStart);
	}

	public long recorreColumnasFilas(int value) {
		cantidad = 0;
		long tsStart = System.currentTimeMillis(); 
		long tsStop = 0;
		for (int i=0; i<MAXcols; i++) {
			for (int j=0; j<MAXrows; j++) {
				if ( this.MATRIX[j][i] == value ) { cantidad++; } 
			}
		}
		tsStop = System.currentTimeMillis();
		return (tsStop-tsStart);
	}	
		
	public static void main(String[] args) {
		StrangesArrays test = new StrangesArrays();
		System.out.println("Time outer rows - inner cols: "+test.recorreFilasColumnas(10));
		System.out.println("Time outer cols - inner rows: "+test.recorreColumnasFilas(10));
	}
	
	
	
	
	
}
