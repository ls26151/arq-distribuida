import java.util.Random;

public class Main {
	
	
	public static void main(String[] args) {

		LamportMutex th1 = new LamportMutex();
		LamportMutex th2 = new LamportMutex();
		
		th1.start();
		th2.start();
		
		try {
			th1.join();
			th2.join();
		}catch (Exception e){}
		
		System.out.println("Valor: "+LamportMutex.CHECK);
		
		
	}

}
