package Utils;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.Random;

public class GFunctions {
	
	
	
	public static String cleanNullString(String inputString) {
		String outString = "";
		if ( inputString == null ) return outString; 
		if ( inputString.equals("null") ) return outString;
		if ( inputString.equals("(null)") ) return outString;
		return inputString.trim();
	}

	public static void writeToScreen(String message) {
		System.out.println(message);
		GFunctions.writeToLog(message);
	}

	public static int max(int value1,int value2) {
		if ( value1 > value2 ) { return value1; }
		else { return value2; }
	}
	
	public static void writeToLog(String message) {
		try {
			DataOutputStream fileLog = new DataOutputStream( new BufferedOutputStream( new FileOutputStream("lamportlog",true) ) );
			long sysTS = GFunctions.getSystemTimestamp();
			fileLog.writeBytes(sysTS+";"+message+"\n");
			fileLog.close();
		}catch (Exception e) {}
	}
	
	public static void sleep(int ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {
			try {
				GFunctions.writeToLog("Error realizando un sleep de "+ms+"ms --> "+e.getMessage());
			} catch (Exception e1) {
			}
		}
	}
	
	public static long getSystemTimestamp() {
		return System.currentTimeMillis();
	}
	public static int getRandomOffsetMillis() {
		return (new Random()).nextInt(100);
	}
}
