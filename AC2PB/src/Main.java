import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Network.Datos;
import Ricard.MasterRicard;
import Utils.GFunctions;

public class Main {

	public static void main(String[] args) {
		int ITERATIONS = 1;
		int NUMTHREADS = 3;
		
		JLabel[][] labels = new JLabel[NUMTHREADS+1][NUMTHREADS];
		JPanel grid = new JPanel();
		grid.setLayout(new GridLayout(NUMTHREADS+1,NUMTHREADS));
		for (int row=0;row<(NUMTHREADS);row++){
			for (int col=0; col<NUMTHREADS;col++) {
				labels[row][col] = new JLabel("0");
				grid.add(labels[row][col]);
			}
		}
		for (int col=0; col<(NUMTHREADS); col++) { 
			labels[NUMTHREADS][col] = new JLabel(" STATUS ");
			grid.add(labels[NUMTHREADS][col]);
		}
		
		JFrame frame = new JFrame("AC2: Ricard Agrawala Com. Matrix");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(grid);
		frame.pack();
		frame.setLocation(100,300);
		frame.setVisible(true);
		
		Datos.getInstance().setToken("");
		GFunctions.writeToScreen("MASTER: Token --> "+Datos.getInstance().hasToken());
		MasterRicard mRicard = new MasterRicard(NUMTHREADS, ITERATIONS, labels);
		// Lanzamos el Thread
		mRicard.start();
		// Lanzamos el Server
		mRicard.startServer();
	}

}
