package Network;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

import Utils.GFunctions;

public class Notifier {
	private Frame frameTX;
	private String server_host;
	private int server_port;
	private String server_name;
	
	
	public Notifier(String server_name, String host, int port, Frame frame) {
		this.frameTX = frame;
		this.server_name = server_name;
		this.server_host = host;
		this.server_port = port;
	}

	public void send() {
		boolean frame_snd = false;
		boolean frame_ack = false;
		int MAXRETRYS = 20;
		int retrynum = 0;
		do {
			try {
				GFunctions.writeToScreen("[INFO] Starting token send to "+this.server_name);
				Socket lamportServer = new Socket(this.server_host,this.server_port);
				ObjectOutputStream oStream = new ObjectOutputStream(lamportServer.getOutputStream());
				
				
				// Enviamos el token 
				GFunctions.writeToScreen("[INFO] Token send, waitting for ACK");
				oStream.writeObject(frameTX);
				frame_snd = true;
				
				
				// Recibimos el ACK
				ObjectInputStream iStream  = new ObjectInputStream(lamportServer.getInputStream());
				Frame frameRX = (Frame) iStream.readObject();
				frame_ack = frameRX.ack;					
				GFunctions.writeToScreen("[INFO] Frame received");
				if ( frame_ack ) {
					GFunctions.writeToScreen("[INFO] ACK OK deleting token");
					Datos.getInstance().setToken("");
					GFunctions.writeToScreen("[INFO] Token sended ok");
				} else {
					GFunctions.writeToScreen("[INFO] ACK KO!!!");
				}
					
				oStream.close();
				iStream.close();
				lamportServer.close();
				GFunctions.writeToScreen("[INFO] Closing notifier");
			} catch (Exception e) {
				frame_snd = false;
				frame_ack = false;
				GFunctions.writeToLog("NOTIFIER: [ERROR] Cant send token to "+this.server_name);
				GFunctions.sleep(500);
				retrynum++;
			}
		} while ( (retrynum < MAXRETRYS) && (frame_snd == false || frame_ack == false) );
	}
}
