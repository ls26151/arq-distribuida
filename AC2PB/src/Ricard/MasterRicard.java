package Ricard;

import java.awt.GridLayout;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JFrame;
import javax.swing.JLabel;

import Network.Datos;
import Network.Frame;
import Network.Notifier;
import Utils.GFunctions;

public class MasterRicard extends Thread{
	private int numThreads;
	private LightRicardThread[] threads;
	private String[][] matrix;
	private int iterations;
	private JLabel[][] labels;
	
	public MasterRicard(int nThreads, int iterations, JLabel[][] labels) {
		this.numThreads = nThreads;
		this.labels = labels;
		this.iterations = iterations;
		this.matrix = this.cleanComunicationsMatrix(this.generateComunicationsMatrix());
		threads = new LightRicardThread[nThreads];
	}
	
	@Override
	public void run() {
		Lock lock = new ReentrantLock();
		// Lanzamos los threads y se quedaran a la espera de que uno de los procesos tenga el token
		for (int i=0; i<this.numThreads;i++) {
			JFrame frame = new JFrame("Thread "+i);
			JLabel[][] queueLabel = new JLabel[2][numThreads];  
			frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			frame.setLayout(new GridLayout(2,2));
			frame.setLocation(i*300, 600);
			frame.setSize(300, 100);
			for (int j=0; j<2; j++) {
				for (int k=0; k<2;k++) {
					queueLabel[j][k] = new JLabel(" - ");
					frame.add(queueLabel[j][k]);
				}
			}
			
			frame.setVisible(true);
			threads[i] = new LightRicardThread(i,"RIC"+i, iterations, Datos.PSCREEN_HOST, Datos.PSCREEN_PORT,this.matrix, this.labels, queueLabel, lock);
			threads[i].start();
		}
		
		
		while (true) {
			// Esperamos a recibir el token esperando unos 250ms con sleeps
			this.tokenWait(); 
			// Despertamos a los Threads
			this.sendSignalToChild("GO");
			// Esperamos a que todos los Lamports Threads hayan notificado su finalizacion
			this.ricardThreadWait();
			// Cuando todos han acabado, les decimos que pueden resetearse
			this.sendSignalToChild("RST");
			// Pregunta? Es necesario limpiar la matriz de comunicaciones
			this.cleanComunicationsMatrix(this.matrix);
			// Enviamos TOKEN a RICARD Server
			Frame frameTX = new Frame("MASRICARD", Datos.getInstance().getToken(), "" ,Frame.FRAME_TOKEN_SND, false);
			Notifier notifier = new Notifier("MASLAMPORT", Datos.PLAMPOR_HOST, Datos.PLAMPOR_PORT, frameTX);
			notifier.send();
		}
	}

	public void startServer() {
		try {
			ServerSocket server = new ServerSocket(Datos.PRICARD_PORT);
			Socket csocket = null;
			while ( true ) {
				csocket = server.accept();
//				GFunctions.writeToScreen("MASTER: Conexion aceptada");
				ObjectInputStream iStream = new ObjectInputStream(csocket.getInputStream());
//				GFunctions.writeToScreen("MASTER: Creado inputstream");
				// Ya que solo vamos a recibir 1 trama cuando tengamos que recibir el token, no hace falta que sea multithreading server
//				GFunctions.writeToScreen("MASTER: Esperando lectura de objeto");
				Frame frameRX = (Frame) iStream.readObject();
//				GFunctions.writeToScreen("MASTER: Objeto leido");
				this.handleMessage(frameRX, csocket);
				
				// Una vez gestionada la trama, cerramos conexiones y esperamos a nueva conexion
				iStream.close();
				if ( csocket.isClosed() == false ) {
					csocket.close();
				}
			}
		} catch (Exception e) {
			GFunctions.writeToScreen("MASTER: No se ha podido iniciar el servidor");
			e.printStackTrace();
		}
	}
	
	public String[][] generateComunicationsMatrix() {
		String[][] matrix = new String[numThreads+1][numThreads];
		return matrix;
	}
	
	private String[][] cleanComunicationsMatrix(String[][] matrix) {
		for (int i=0; i<numThreads;i++) {
			for (int j=0; j<numThreads; j++) {
				matrix[i][j] = "0";
				labels[i][j].setText("0");
			}
			matrix[numThreads][i] = "";
		}
		return matrix;
	}
	
	private void handleMessage(Frame frameRX,Socket client) {
		GFunctions.writeToScreen("Gestionando la peticion de conexion");
		switch(frameRX.type) {
			case Frame.FRAME_TOKEN_SND:
				if ( Datos.getInstance().hasToken() == false && frameRX.token != null && !frameRX.token.trim().equals("") ) {
					
					frameRX.ack = true;
					GFunctions.writeToScreen("Valor de ACK que enviaremos "+frameRX.ack);
					try {
						ObjectOutputStream oStream = new ObjectOutputStream(client.getOutputStream());
						oStream.writeObject(frameRX);
						oStream.close();
						Datos.getInstance().setToken(frameRX.token);
						GFunctions.writeToScreen("Setted token "+frameRX.token+" Instance Token ["+Datos.getInstance().hasToken()+"]--> "+Datos.getInstance().getToken());
					} catch (IOException e) {
						GFunctions.writeToScreen("MASTER: Error al enviar la trama de respuesta con ACK TRUE");
					} 
				} else {
					GFunctions.writeToScreen("ENTRO EN EL ELSE");
					try {
						ObjectOutputStream oStream = new ObjectOutputStream(client.getOutputStream());
						oStream.writeObject(frameRX);
						oStream.close();
						Datos.getInstance().setToken(frameRX.token);
					} catch (Exception e) {
						GFunctions.writeToScreen("MASTER: Error al enviar la trama de respuesta con ACK FALSE");
					}
				}
			break;
			default:
			break;
		}			
	}
	
	private void tokenWait() {
		
		while (Datos.getInstance().hasToken() == false ) {
//			GFunctions.writeToScreen("MASTER: Esperando el token");
			GFunctions.sleep(500);
		}
	}
	private void ricardThreadWait() {
		boolean salir = true;
		do {
			salir = true;
			for (int j=0; j<this.numThreads;j++) {
				// Si no se cumple que todos los Lamport han acabado, seguimos a la espera!
				salir = (salir && this.matrix[numThreads][j].equalsIgnoreCase("END") );
			}
			GFunctions.sleep(500);
		} while (salir == false);
		GFunctions.writeToScreen("MASTER: Los LW Ricard han finalizado");
	}
	
	private void sendSignalToChild(String text) {
		// Vamos a randomizar el despertar

		if (text.equals("GO") ) {
			ArrayList<Integer> countdown = new ArrayList<Integer>();
			for (int j=0; j<numThreads;j++) { countdown.add(j); }

			Random rnd = new Random();
			while ( countdown.isEmpty() == false ) {
				int rand = rnd.nextInt(countdown.size());
				this.matrix[numThreads][countdown.get(rand)] = text;
				this.labels[numThreads][countdown.get(rand)].setText(text);	
				
				GFunctions.writeToLog("MASTER: Sending Wakeup to TH"+rand);
				countdown.remove(rand);
				GFunctions.sleep(50); 

			}
			
			GFunctions.writeToLog("MASTER: Despertando a los LW Lamports");
		} else {
			for (int i=0; i<numThreads;i++) {
				this.matrix[numThreads][i] = text;
				this.labels[numThreads][i].setText(text);
			}
		}
	}
}
