package Ricard;

import Utils.GFunctions;
class LamportClock {
	int c;
	public LamportClock() {
		c = 1;
	}
	
	public int getValue() {
		return c;
	}
	
	public void tick() {
		c++;
	}
	
	public void receiveAction(int src, int sentValue) {
		c = GFunctions.max(c, sentValue)+ 1;
	}
	public void reset() {
		c = 1;
	}
}
