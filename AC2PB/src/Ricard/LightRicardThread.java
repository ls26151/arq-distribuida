package Ricard;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.locks.Lock;

import javax.swing.JLabel;

import Network.Datos;
import Network.Frame;
import Utils.GFunctions;

public class LightRicardThread extends Thread {
	private int id;
	private final int MAX;
	private String nodename; 
	
	private String comMatrix[][];
	private int matrixCols;
	private int matrixRows;
	
	private RicardMutex mutex;
	private String server_host;
	private int server_port;
	private JLabel[][] labels;
	private Lock lock;
	
	public LightRicardThread(int id, String name, int iterations,String server_host, int server_port, String[][] comMatrix, JLabel[][] labels, JLabel[][] queueLabel, Lock lock) {
		this.id = id;
		this.nodename = name;
		this.MAX = iterations;
		this.server_host = server_host;
		this.server_port = server_port;
		this.comMatrix = comMatrix;
		this.matrixRows = comMatrix.length;
		this.matrixCols = comMatrix[0].length;
		this.labels = labels;
		this.lock = lock;
		this.mutex = new RicardMutex(this.id,this.comMatrix,labels,queueLabel,lock);
	}
	
	@Override
	public void run() {
		int MAXPRINTS = 10;
		while (true) {
			int i = 0;
			this.fatherSignalWait();
			while (i<MAX) {
				int j = 0;
				mutex.requestCS();
				GFunctions.writeToLog(this.nodename+" - Inside Critical Section");
				while (j<MAXPRINTS) {
					try {
//						GFunctions.writeToScreen("TH"+this.nodename+" - Escribiendo ["+i+"] "+j+"/"+MAXPRINTS);
						Socket screenServer = new Socket(Datos.PSCREEN_HOST,Datos.PSCREEN_PORT);
						ObjectOutputStream oStream = new ObjectOutputStream(screenServer.getOutputStream());
						ObjectInputStream iStream = new ObjectInputStream(screenServer.getInputStream());
						
						Frame frameTX = new Frame(this.nodename,"","Soc el process lightweight "+this.nodename+" - "+j+"]",0,false);
						oStream.writeObject(frameTX);
						frameTX = (Frame) iStream.readObject();
						oStream.close();
						iStream.close();
						screenServer.close();
						GFunctions.sleep(1000);
						j++;
					} catch (Exception e) {
						GFunctions.writeToLog("TH"+this.nodename+" Frame sending failure with value "+j);
					}					
				}
				GFunctions.writeToLog(this.nodename+" - Leaving Critical Section");
				mutex.releaseCS();
				i++;
			}
			this.fatherSignalSend();
	
		}
	}
	private void fatherSignalWait() {
		
//		GFunctions.writeToScreen("TH"+this.id+" - Waitting father signal");
		boolean salir = false;
		while (salir == false) {
			GFunctions.sleep(750);
			if ( this.comMatrix[this.matrixRows-1][this.id].equals("GO") ) { 
				salir = true;
			} else if ( this.comMatrix[this.matrixRows-1][this.id].equals("RST") ) {
				this.mutex.reset();
			}
			mutex.readMessages();
		}
	}
	
	private void fatherSignalSend() {
		this.comMatrix[this.matrixRows-1][this.id] = "END";
		this.labels[this.matrixRows-1][this.id].setText("END");		
	}
}
