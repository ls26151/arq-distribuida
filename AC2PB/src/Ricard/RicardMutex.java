package Ricard;

import java.util.LinkedList;
import java.util.concurrent.locks.Lock;

import javax.swing.JLabel;

import Utils.GFunctions;
import Utils.Simbols;

public class RicardMutex {

	private int myID;
	private int myTs;
	private String[][] matrix;
	private JLabel[][] labels;
	private JLabel[][] guiLabels;
	private int size;
	private Lock lock;
	private LamportClock clock;
	private int numOkay;
	private LinkedList<Integer> pendingQ;
	
	public static final int REQUEST = 10;
	public static final int RELEASE = 20;
	
	public RicardMutex(int myID,String[][] matr, JLabel[][] matrixLabels, JLabel[][] queueLabel, Lock lock) {
		this.myID = myID;
		this.matrix = matr;
		this.size = matrix[0].length;
		this.myTs = Simbols.Infinity;
		this.labels = matrixLabels;
		this.lock = lock;
		this.clock = new LamportClock();
		this.pendingQ = new LinkedList<Integer>(); //--> Lo que se guardara seran la posicion de los threads
		this.guiLabels = queueLabel;
		this.reset();
	}
	
	public void requestCS() {
		clock.tick();
		this.myTs = clock.getValue();
		this.broadcastMessage(REQUEST,myTs);
		this.numOkay = 0;
		this.updateGui();
		while (numOkay < (size - 1) ) {
			this.readMessages();
			this.myWait();
		}
	}
	
	public void releaseCS() {
		this.myTs = Simbols.Infinity;
		while ( !pendingQ.isEmpty() ) {
			int thid = pendingQ.removeFirst();
			this.sendMessage(thid, RELEASE+"@"+clock.getValue());
		}
		this.updateGui();
	}
		
	private void myWait() {
		GFunctions.sleep(250);
	}
	
	public void readMessages() {
		boolean mensajesSinProcesar = false;
//		if ( this.myID == 0 ) GFunctions.writeToScreen("TH"+this.myID+" Entro en readMessages");
		for (int i=0; i<this.size; i++) {
//			if ( this.myID == 0 ) GFunctions.writeToScreen("TH"+this.myID+" Bucle de lectura con i == "+i);
			this.handleMessage(this.matrix[i][this.myID], i);
			if ( this.messageSinProcesar(i) == true ) { mensajesSinProcesar = true;	}
			if ( (i+1) == size && mensajesSinProcesar == true ) {
//				if ( this.myID == 0 ) GFunctions.writeToScreen("TH"+this.myID+" Reseteo");
				i=0;
				mensajesSinProcesar = false;
				GFunctions.sleep(100);
			}
		}
	}
	private boolean messageSinProcesar(int pos) {
		int index = this.matrix[this.myID][pos].indexOf("@");
		if ( index != -1 ) {
			return true;
		}
		return false;
	}

	private void handleMessage(String message, int sender) {
		if ( message.indexOf("@") != -1 ) {
			
			String[] values = message.split("@");
			int typeMessage = Integer.parseInt(values[0]);
			int timestamp = Integer.parseInt(values[1]);
			GFunctions.writeToLog("TH"+this.myID+" Hay un mensaje para mi del TH"+sender+"! ");
			clock.receiveAction(sender, timestamp);
			if (typeMessage == REQUEST ) {
				if ( myTs == Simbols.Infinity || (timestamp < myTs) || ((timestamp == myTs) && (sender < this.myID)) ) {
					this.sendMessage(sender, RELEASE+"@"+clock.getValue());
				} else {
					this.pendingQ.add(sender);
				}
			} else if (typeMessage == RELEASE) {
				numOkay++;
				GFunctions.writeToLog("Recibido otro OKAY --> "+numOkay);
			}
			this.lock.lock();
				this.matrix[sender][this.myID] = "0";	
				this.lock.unlock();
				this.labels[sender][this.myID].setText("0");
				this.updateGui();
		}
	}
	
	private void broadcastMessage(int typeofmessage, int value) {
		switch(typeofmessage) {
			case REQUEST:
				for (int i=0; i<this.size;i++) {
					if ( i != this.myID ){
						this.lock.lock();
							this.matrix[this.myID][i] = REQUEST+"@"+clock.getValue();
						this.lock.unlock();
							this.labels[this.myID][i].setText(this.matrix[this.myID][i]); 
					}
				}
			break;
			default:
			break;
		}
	}
	private void sendMessage(int thid, String message) {
		this.matrix[this.myID][thid] = message;
	}
	
	public void reset() {
		this.guiLabels[0][0].setText("My TStamp");
		this.guiLabels[0][1].setText("RA Clock");
		this.clock.reset();
		this.updateGui();
		
	}
	private void updateGui() {
		this.guiLabels[1][0].setText(myTs+"");
		this.guiLabels[1][1].setText(clock.getValue()+"");
	}
}
